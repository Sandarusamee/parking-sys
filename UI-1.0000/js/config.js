var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/thymeleaf'),
    content: require('/lib/xp/content'),
    util: require('/lib/util'),
    commonUtils: require('/lib/common-utils'),
    session : require('/lib/session'),
    constants: require('/lib/constants')
};

let view = resolve('travel-wapp-web-inner.html');
let innerPaths = libs.constants.navigation.INNER_PATHS;
let siteConfig = libs.commonUtils.getSiteSettings();

//Handle Get request
exports.get = function (req) {
    var content = libs.portal.getContent();
    var mainRegion = isFragment ? null : content.page.regions.main;
    var site = libs.portal.getSite();

    let reference = req.cookies.reference;
    let mainTree = libs.commonUtils.getMainTree(reference);
    if(req.path == '/dashboard' || req.path == '/my-account'){
        let persistedPersonReference = libs.commonUtils.getPersonReference(mainTree);
        mainTree = libs.commonUtils.getCurrentMainTree(reference, persistedPersonReference);
    }
    let activeQuote = libs.commonUtils.getActiveQuote(mainTree);
    let email_verified = libs.commonUtils.hasEmailVerified(mainTree);
    let quote_id = activeQuote.quoteReference;
    let titleValue =  content.page.config.titleValue;
    let screenName =  content.page.config.screenName;
    if(libs.commonUtils.getOnTripStatus(activeQuote)){
        if(screenName == 'ManageSubscriptionOffTripWeb'){
            screenName = 'ManageSubscriptionOnTripWeb';
        }else if(screenName == 'OffTripDashboardWeb'){
             screenName = 'OnTripDashboardWeb';
        }else if(screenName == 'HelpCenterWeb'){
              screenName = 'OnTripHelpWeb';
        }else if(screenName == 'MyAccountWeb'){
              screenName = 'OnTripHelpWeb';
        }
    }

    let hasLoggedIn = libs.commonUtils.hasLoggedIn(reference);
    let personReference = mainTree.person?mainTree.person.personReference:'';
    let quoteId = mainTree.quotes?mainTree.quotes.quoteReference:'';
    let name = mainTree.person?(mainTree.person.firstName+' '+mainTree.person.lastName):'';
    let customerType = hasLoggedIn?'existing':'prospect’';

    let showWappVersion = libs.commonUtils.getSiteSettings()['showWappVersion'] || false;
    let appsFlyerOneLink = libs.commonUtils.getSiteSettings()['appsFlyerOneLink'] || false;
    let zenDeskEnabled = libs.commonUtils.getSiteSettings()['zenDeskEnabled'] || false;
    let cookieConsentEnabled = libs.commonUtils.getSiteSettings()['cookieConsentEnabled'] || false;

    let wappVersion = app.version.split("-")[0];
    let tripstatus='';
    let journeyType='';
    if (innerPaths.indexOf(req.path) > -1 || (libs.session.getAttribute(reference, "isQuoteFlow") || !hasLoggedIn)){
        tripstatus = "Quote-Flow";
        journeyType = "registration";
    } else {
        if (libs.commonUtils.isOnTrip(activeQuote)) {
             tripstatus = "on-trip";
             journeyType = "on-trip";
        } else {
             tripstatus = "off-trip";
             journeyType = "off-trip";
        }
    }

    // Fragment handling (single fragments should use this page controller automatically to render itself)
    var isFragment = content.type === 'portal:fragment';
    let cookieConsent = {
                           "necessary":[
                              "AWSALB",
                              "AWSALBCORS",
                              "reference",
                              "verify_token",
                              "TOKENCOOKIECSRF",
                              "purecookieDismiss"
                           ],
                           "functionality":[
                              "__cfruid",
                              " ZD-buid",
                              "ZD-suid"
                           ],
                           "performance":[
                              "_ga_#",
                              "_ga",
                              "afUserId",
                              "af_id",
                              "AF_SYNC",
                              "AF_SESSION",
                              "619533cfaecfb700e12a99e1.clientId"
                           ],
                           "marketing":[
                              "_gcl_au",
                              "_fbp",
                              "tt_sessionId",
                              "_ttp",
                              "_tt_enable_cookie",
                              "tt_appInfo"
                           ]
                        }
    if(JSON.parse(libs.commonUtils.getKeyValue('cookie-consent').data.value).necessary){
        cookieConsent =  JSON.parse(libs.commonUtils.getKeyValue('cookie-consent').data.value);
    }
    var model = {
        showWappVersion,
        appsFlyerOneLink,
        wappVersion,
        mode: req.mode,
        lang : site.language || "en-us",
        mainRegion: mainRegion,
        isFragment: isFragment,
        hasLoggedIn,
        isQuoteFlow: libs.session.getAttribute(reference, "isQuoteFlow") && innerPaths.indexOf(req.path) == -1 || !hasLoggedIn,
        isOnTrip: libs.commonUtils.isOnTrip(activeQuote),
        hasAnActivePolicy: libs.commonUtils.hasAnActivePolicy(activeQuote),
        onTripStatus: libs.commonUtils.getOnTripStatus(activeQuote),
        titleValue,
        email_verified,
        gtmSnippet:siteConfig['gtm-snippet'],
        gtmSnippetNoscript:siteConfig['gtm-snippet-noscript'],
        appflyerSnippet:siteConfig['appflyer-snippet'],
        appflyerSnippetNoCookie:siteConfig['appflyer-snippet-no-cookie'],
        personReference,
        name,
        site,
        tripstatus,
        quote_id,
        env: app.config.mode,
        journeyType,
        displayRAF: siteConfig['displayRAF'] || false,
        cancelledTrip : libs.session.getAttribute(reference, "mainTree.quotes.indicators.tripCancelStatus"),
        quoteId,
        customerType,
        screenName,
        enableAppflyer:siteConfig['enableAppsFlyer'],
        zenDeskEnabled,
        cookieConsentEnabled,
        necessaryCookies : cookieConsent.necessary,
        functionalityCookies : cookieConsent.functionality,
        performanceCookies : cookieConsent.performance,
        marketingCookies : cookieConsent.marketing,
    };
    if ((req.mode == 'inline')||(req.mode == 'preview')||(req.mode == 'edit')){
    if((req.path.indexOf('dashboard') > -1)||(req.path.indexOf('my-account') > -1)||(req.path.indexOf('my-account-settings') > -1)||(req.path.indexOf('manage-people') > -1)||(req.path.indexOf('manage-subscription') > -1)||(req.path.indexOf('help-centre') > -1)){
        model.isQuoteFlow = false;
    }
    }
    if((req.path.indexOf('help-centre') > -1) || model.isQuoteFlow){
        model.zenDeskEnabledPage = true;
    }
    var body = libs.thymeleaf.render(view, model);
    return {body: body};
};
