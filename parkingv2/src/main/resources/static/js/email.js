<script  type="text/javascript">
 var frmvalidator = new Validator("company");
 frmvalidator.addValidation("name","req","Please enter your company Name");
 frmvalidator.addValidation("name","maxlen=20");
 
 frmvalidator.addValidation("LastName","req");
 frmvalidator.addValidation("LastName","maxlen=20");
 
 frmvalidator.addValidation("email","maxlen=50");
 frmvalidator.addValidation("email","req");
 frmvalidator.addValidation("email","email");

 
 frmvalidator.addValidation("contactno","maxlen=10");
 frmvalidator.addValidation("contactno","numeric");
 
 frmvalidator.addValidation("Password","minlen=8");
 


</script>
