package com.example.parkingv1.service;

import com.example.parkingv1.dto.LocationDTO;
import com.example.parkingv1.model.ParkinglocationEntity;
import com.example.parkingv1.repository.ParkingLocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.plugin.javascript.navig.Array;

import java.util.ArrayList;

@Service
public class NearestParking{
    @Autowired
    Haversine haversine;
    @Autowired
    GetLonLat getLonLat;
    @Autowired
    LocationDTO locationDTO;
    @Autowired
    ParkingLocationRepo parkingLocationRepo;
int p;
    ParkinglocationEntity parkinglocationEntity;
    public double nearestParking(int locationid ,double crlat,double crlon){
        int i;

      double parklat =  getLonLat.getLat(locationid);
        double parklon =  getLonLat.getLon(locationid);


        //ArrayList<Double> distances  = new ArrayList<>();
               double distances = haversine.findDistance(crlat,crlon,parklat,parklon);
      return distances;
    }

   public int[] shorestDistances(int[] locationid,double clat,double clon){
        int i;
        int j=0;
        int locid[]= new int[10];
      int k= parkingLocationRepo.findaids().length;
        double [] distance= new double[k];

        for(i=0;i<k;i++) {

            distance[i]=  nearestParking(locationid[i],clat, clon);




            if(distance[i]<100&&(parkingLocationRepo.findavailableslotsByID(locationid[i] )>0)){
                if(locationid[i]==0)
                    continue;
                locid[j++]= locationid[i];


            }

           // int [] finalid= new int[locid.length];
        }


        return locid;

    }
    public int[] getloids(double clat,double clon ){
      int matchid[]= shorestDistances(parkingLocationRepo.findaids(),clat,clon);

        return matchid;
    }


}


