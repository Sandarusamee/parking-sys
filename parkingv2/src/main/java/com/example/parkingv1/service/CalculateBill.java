package com.example.parkingv1.service;

import com.example.parkingv1.repository.ParkingSlotRepo;
import com.example.parkingv1.repository.ReservationRepo;
import com.example.parkingv1.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculateBill {
    @Autowired
    ReservationRepo reservationRepo;
    @Autowired
    ParkingSlotRepo parkingSlotRepo;
    private int reservationId;
    private String startTime;
    private String endTime;
    private String extendtime;
    private double rateperhour;
    private String slotid;
    @Autowired
    UserRepo userRepo;

    public int getUserid(int reservationId){
        int userid= reservationRepo.findUseridByReservationId(reservationId);
        return userid;
    }
    public int reservedTime(int reservationId){try {


        Integer endTime = Integer.parseInt(reservationRepo.findEndtimeById(reservationId));
        Integer startTime = Integer.parseInt(reservationRepo.findStarttimeById(reservationId));
        Integer extendtime = Integer.parseInt(reservationRepo.findExtendtimeById(reservationId));
        int reservedtime= (endTime-startTime)+extendtime;
        return reservedtime;}catch (NumberFormatException nfe){
        System.out.println("Cannot parse");
        return 0;
    }

    }
   public double calculation(int reservationId,int slotid)throws Exception{
     try{

       Integer endTime = Integer.parseInt(reservationRepo.findEndtimeById(reservationId));
         Integer startTime = Integer.parseInt(reservationRepo.findStarttimeById(reservationId));
         Integer extendtime = Integer.parseInt(reservationRepo.findExtendtimeById(reservationId));
       Double rateperhour = Double.parseDouble(parkingSlotRepo.findChargeperHourbyId(slotid));
         int reservedtime= (endTime-startTime)+extendtime;
         double finalbillamount= ((endTime-startTime)+extendtime)*rateperhour;
         return finalbillamount;



        }
         catch (NumberFormatException nfe){
            System.out.println("Cannot parse");
            return 0;
         }




}}
