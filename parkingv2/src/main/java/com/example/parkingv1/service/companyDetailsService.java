package com.example.parkingv1.service;

import com.example.parkingv1.model.ClientcompanyEntity;
import com.example.parkingv1.model.UsersEntity;
import com.example.parkingv1.repository.CompanyRepo;
import com.example.parkingv1.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
@Service
public class companyDetailsService implements UserDetailsService {
    @Autowired
    private CompanyRepo companyRepo;


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        ClientcompanyEntity activeUser = companyRepo.findCompanyByUsername(userName);
        GrantedAuthority authority = new SimpleGrantedAuthority(activeUser.getRole());
        UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(activeUser.getUserName(),
                activeUser.getPassword(), Arrays.asList(authority));

        return userDetails;
    }
}