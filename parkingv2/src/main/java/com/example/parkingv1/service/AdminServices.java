package com.example.parkingv1.service;

import com.example.parkingv1.model.ClientcompanyEntity;
import com.example.parkingv1.repository.CompanyApprovedRepository;
import com.example.parkingv1.repository.CompanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

@Service
public class AdminServices {
    @Autowired
    CompanyRepo companyRepo;

    @Autowired
    CompanyApprovedRepository companyApprovedRepository;


    public void addToPermenantTable(int registrationNo){
        ArrayList<ClientcompanyEntity> clientcompanyEntities = new ArrayList<>();
        Iterator iterator = companyRepo.findAll().iterator();

        while (iterator.hasNext()){
            ClientcompanyEntity client  = (ClientcompanyEntity) iterator.next();
            clientcompanyEntities.add(client);
        }

        ClientcompanyEntity companybuff =
                clientcompanyEntities.stream().filter(t -> t.getRegistrationNo()==registrationNo).findFirst().get();
        int clientId = companybuff.getCompanyid();

       // ClientcompanyEntity companybuff  = companyRepo.findByRegitrationNo(registrationNo); //store in a buffer
        //ClientcompanyEntity aaa = new ClientcompanyEntity();
        companyRepo.delete(clientId); //delete company

        companyApprovedRepository.save(companybuff);

    }
}
