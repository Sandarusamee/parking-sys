package com.example.parkingv1.service;

import com.example.parkingv1.repository.ParkingLocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GetLonLat {

    @Autowired
    ParkingLocationRepo parkingLocationRepo;
    @Autowired
    NearestParking nearestParking;

    public double getLon(int locationid) {


          double  lon = parkingLocationRepo.findLonCordinatebyId(locationid);

return lon;

    }
    public double getLat(int locationid) {
        double  lat = parkingLocationRepo.findLatCordinatebyId(locationid);
        return lat;
    }
 /*  public double[] matchedlat(){
        int p;
        int[] id=nearestParking.getloids();
        double[] lat =new double[id.length];
        for(p=0;p<id.length;p++) {
                if(id[p]!=0)
            lat[p]= parkingLocationRepo.findLatCordinatebyId(id[p]);

        }
        return lat;
    }
    public double[] matchedlot(){
        int q;
        int[] id=nearestParking.getloids();
        double[] lon =new double[id.length];
        for(q=0;q<id.length;q++) {
            if(id[q]!=0)
            lon[q]= parkingLocationRepo.findLatCordinatebyId(id[q]);

        }
        return lon;
    }*/
}


