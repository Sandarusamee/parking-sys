package com.example.parkingv1.dto;

import java.util.Objects;

public class UserDTO {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(name, userDTO.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }
}
