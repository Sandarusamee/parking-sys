package com.example.parkingv1.dto;

import java.io.Serializable;

public class parkingDTO implements Serializable {

    private Integer locationId;
    private String name;
    private Integer companyId;
    private String longitudecordinate;
    private String latitudecordinate;
    private int noofslots;
    private Double distance;

    public parkingDTO(int locationId, String latitudecordinate)
    {

    }
    public parkingDTO(Integer id,String lat)
    {
        this.locationId = id;
        this.latitudecordinate = lat;
    }

    public int getLocationId(int locationId) {
        return this.locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getLongitudecordinate() {
        return longitudecordinate;
    }

    public void setLongitudecordinate(String longitudecordinate) {
        this.longitudecordinate = longitudecordinate;
    }

    public String getLatitudecordinate(String latitudecordinate) {
        return this.latitudecordinate;
    }

    public void setLatitudecordinate(String latitudecordinate) {
        this.latitudecordinate = latitudecordinate;
    }


    public int getNoofslots() { return noofslots; }
    public void setNoofslots(int noofslots) { this.noofslots = noofslots; }


    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        parkingDTO parkingdto = (parkingDTO) o;

        return locationId != null ? locationId.equals(parkingdto.locationId) : parkingdto.locationId == null;
    }

    @Override
    public int hashCode() {
        return locationId != null ? locationId.hashCode() : 0;
    }
}