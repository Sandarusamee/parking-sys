package com.example.parkingv1.dto;

import com.example.parkingv1.controller.LocationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Objects;
@Service
public class LocationDTO  {

    double currentlat;
    double currentlon;
    double lat;
    double lon;
    public double getCurrentlat() {
        return currentlat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationDTO that = (LocationDTO) o;
        return Double.compare(that.currentlat, currentlat) == 0 &&
                Double.compare(that.currentlon, currentlon) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(currentlat, currentlon);
    }

    public void setCurrentlat(double currentlat) {
        this.currentlat = currentlat;

    }

    public double getCurrentlon() {
        return currentlon;
    }

    public void setCurrentlon(double currentlon) {
        this.currentlon = currentlon;
    }
}