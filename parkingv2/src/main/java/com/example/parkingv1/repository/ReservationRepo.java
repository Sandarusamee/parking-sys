package com.example.parkingv1.repository;

import com.example.parkingv1.model.ReservationEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepo extends CrudRepository<ReservationEntity,Integer>{
public ReservationEntity findReservationEntitiesByStartTime(String time);
public ReservationEntity findAllByDate(String date);
    @Query(value="SELECT endTime FROM ReservationEntity u WHERE u.reservationId=?1")
    public String findEndtimeById(int reservationId);

    @Query(value="SELECT startTime FROM ReservationEntity u WHERE u.reservationId=?1")
    public String findStarttimeById(int reservationId);

    @Query(value="SELECT userid FROM ReservationEntity u WHERE u.reservationId=?1")
    public int findUseridByReservationId(int reservationId);


    @Query(value="SELECT extendtime FROM ReservationEntity u WHERE u.reservationId=?1")
    public String findExtendtimeById(int reservationId);

}
