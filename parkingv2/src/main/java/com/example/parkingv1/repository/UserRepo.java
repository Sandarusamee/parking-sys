package com.example.parkingv1.repository;

import com.example.parkingv1.model.UsersEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends CrudRepository<UsersEntity,Integer> {


    @Query(value="SELECT u FROM UsersEntity u WHERE u.name=?1")
    UsersEntity findUserByUsername(String name);
    @Query(value="SELECT u FROM UsersEntity u WHERE u.name=?1")
    Integer findIdByUsername(String name);
    @Query(value="SELECT u FROM UsersEntity u WHERE u.name=?1")
    UsersEntity findUserByUsernamee(String name);
    //UsersEntity findPasswordByUsername(String password);
    //for admin
    @Query(value="SELECT v FROM UsersEntity v WHERE v.userid=?1")
    UsersEntity findUserByUserid(int userid);

    //for admin
    @Query(value = "SELECT u FROM UsersEntity u WHERE u.name=?1")
    UsersEntity findUserByName(String name);

}
