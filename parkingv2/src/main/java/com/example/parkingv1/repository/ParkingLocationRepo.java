package com.example.parkingv1.repository;

import com.example.parkingv1.model.ParkinglocationEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingLocationRepo extends CrudRepository <ParkinglocationEntity,Integer>{
    public ParkinglocationEntity findByName(String name);
    @Query(value="SELECT longitudecordinate FROM ParkinglocationEntity u WHERE u.locationId=?1")
    public Double findLonCordinatebyId(int locationId);
    @Query(value="SELECT latitudecordinate FROM ParkinglocationEntity u WHERE u.locationId=?1 ")
    public Double findLatCordinatebyId(int locationId);
    @Query(value="SELECT noofavailableslots FROM ParkinglocationEntity u WHERE u.locationId=?1 ")
    public int findavailableslotsByID(int locationId);
    @Query(value="SELECT locationId FROM ParkinglocationEntity u ")
    public int[] findaids();
    //@Query(value="SELECT rateperhour FROM ParkinglocationEntity u WHERE u.locationId=?1")
  //  public int[] findRateByID(int id);
   // @Query(value="SELECT u FROM ParkinglocationEntity u WHERE u.locationId=?1")
  //  public ParkinglocationEntity findParkingsByID();
}
