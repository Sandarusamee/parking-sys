package com.example.parkingv1.repository;

import com.example.parkingv1.model.ClientcompanyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepo extends CrudRepository<ClientcompanyEntity,Integer> {
    @Query(value="SELECT u FROM ClientcompanyEntity u WHERE u.name=?1")
    public ClientcompanyEntity findByName(String name);

    public ClientcompanyEntity findAllBy(int id);
    @Query(value="SELECT u FROM ClientcompanyEntity u WHERE u.userName=?1")
    ClientcompanyEntity findCompanyByUsername(String name);
    @Query(value="SELECT u FROM ClientcompanyEntity u WHERE u.registrationNo=?1")
    ClientcompanyEntity findByRegitrationNo(int registrationNo);


}