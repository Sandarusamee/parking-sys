package com.example.parkingv1.repository;

import com.example.parkingv1.model.ParkingSlotEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingSlotRepo extends CrudRepository<ParkingSlotEntity,Integer> {

    @Query(value="SELECT rateperhour FROM ParkingSlotEntity u WHERE u.slotId=?1")
    public String findChargeperHourbyId(int slotId);
 //   @Query(value="SELECT noofslots FROM ParkingSlotEntity u WHERE u.slotId=?1")
   // public String findId(int slotId);
}
