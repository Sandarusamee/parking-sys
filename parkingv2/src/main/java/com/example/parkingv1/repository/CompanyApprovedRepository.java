package com.example.parkingv1.repository;

import com.example.parkingv1.model.ClientcompanyEntity;
import com.example.parkingv1.model.ClientcompanyEntityApproved;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyApprovedRepository extends CrudRepository<ClientcompanyEntityApproved,Integer> {

    //@Query(value="SELECT u FROM ClientcompanyEntityApproved u WHERE u.userName=?1")
    //ClientcompanyEntityApproved findCompanyByUsername(String name);

    void save(ClientcompanyEntity companybuff);

    // @Query(value = "INSERT INTO ClientcompanyEntityApproved (registrationNo2,name2,contactNo2,userName2,password2,email2,blacklistDetails2,noOfSlots2,companyid2,role2) VALUES(companybuff.registrationNo2,companybuff.name2,companybuff.contactNo2,companybuff.userName2,companybuff.password2,companybuff.email2,companybuff.blacklistDetails2,companybuff.noOfSlots2,companybuff.companyid2,companybuff.role2) ")
   // void save(ClientcompanyEntity companybuff);







}
