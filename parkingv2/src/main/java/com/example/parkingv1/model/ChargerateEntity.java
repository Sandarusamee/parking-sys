package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "chargerate", schema = "mora", catalog = "mora_test")
public class ChargerateEntity {
    private Double chargeAmount;
    private int chargeId;
    private String vehicletype;

    @Basic
    @Column(name = "chargeAmount")
    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    @Id
    @Column(name = "chargeId")
    public int getChargeId() {
        return chargeId;
    }

    public void setChargeId(int chargeId) {
        this.chargeId = chargeId;
    }

    @Basic
    @Column(name = "vehicletype")
    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChargerateEntity that = (ChargerateEntity) o;

        if (chargeId != that.chargeId) return false;
        if (chargeAmount != null ? !chargeAmount.equals(that.chargeAmount) : that.chargeAmount != null) return false;
        if (vehicletype != null ? !vehicletype.equals(that.vehicletype) : that.vehicletype != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = chargeAmount != null ? chargeAmount.hashCode() : 0;
        result = 31 * result + chargeId;
        result = 31 * result + (vehicletype != null ? vehicletype.hashCode() : 0);
        return result;
    }
}
