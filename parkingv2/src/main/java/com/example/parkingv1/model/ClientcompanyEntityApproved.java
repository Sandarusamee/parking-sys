package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "clientcompanyapproved", schema = "mora", catalog = "mora_test")
public class ClientcompanyEntityApproved {
    private Integer registrationNo2;
    private String name2;
    private String contactNo2;
    private String userName2;
    private String password2;
    private String email2;
    private Boolean blacklistDetails2;
    private Integer noOfSlots2;
    private int companyid2;
    private String role2;
    private int mainId;

    @Basic
    @Column(name = "registrationnoz")
    public Integer getRegistrationNo2() {
        return registrationNo2;
    }

    public void setRegistrationNo2(Integer registrationNo2) {
        this.registrationNo2 = registrationNo2;
    }

    @Basic
    @Column(name = "rolez")
    public String getRole2() {
        return role2;
    }

    public void setRole2(String role2) {
        this.role2 = role2;
    }
    @Basic
    @Column(name = "namez")
    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    @Basic
    @Column(name = "contactnoz")
    public String getContactNo2() {
        return contactNo2;
    }

    public void setContactNo2(String contactNo2) {
        this.contactNo2 = contactNo2;
    }

    @Basic
    @Column(name = "usernamez")
    public String getUserName2() {
        return userName2;
    }

    public void setUserName2(String userName2) {
        this.userName2 = userName2;
    }

    @Basic
    @Column(name = "passwordz")
    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    @Basic
    @Column(name = "emailz")
    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    @Basic
    @Column(name = "blacklistdetailsz")
    public Boolean getBlacklistDetails2() {
        return blacklistDetails2;
    }

    public void setBlacklistDetails2(Boolean blacklistDetails2) {
        this.blacklistDetails2 = blacklistDetails2;
    }

    @Basic
    @Column(name = "noofslotsz")
    public Integer getNoOfSlots2() {
        return noOfSlots2;
    }

    public void setNoOfSlots2(Integer noOfSlots2) {
        this.noOfSlots2 = noOfSlots2;
    }

    @Column(name = "companyidz")
    public int getCompanyid2() {
        return companyid2;
    }

    public void setCompanyid2(int companyid2) {
        this.companyid2 = companyid2;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "main_id")
    public int getMainId() {
        return mainId;
    }

    public void setMainId(int mainId) {
        this.mainId = mainId;
    }
}
