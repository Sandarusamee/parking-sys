package com.example.parkingv1.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "parkinglocation", schema = "mora", catalog = "mora_test")
public class ParkinglocationEntity {
    private int locationId;
    private String name;
    private Integer companyId;
    private String longitudecordinate;
    private String latitudecordinate;
    private int noofslots;
    private int noofavailableslots;
    private String rateperhour;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Id
    @Column(name = "locationid")
    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "rateperhour")
    public String getRateperhour() {
        return rateperhour;
    }

    public void setRateperhour(String rateperhour) {
        this.rateperhour = rateperhour;
    }

    @Basic
    @Column(name = "noofavailableslots")
    public int getNoofavailableslots() {
        return noofavailableslots;
    }

    public void setNoofavailableslots(int noofavailableslots) {
        this.noofavailableslots = noofavailableslots;
    }

    @Basic
    @Column(name = "companyid")
    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "longitudecordinate")
    public String getLongitudecordinate() {
        return longitudecordinate;
    }

    public void setLongitudecordinate(String longitudecordinate) {
        this.longitudecordinate = longitudecordinate;
    }

    @Basic
    @Column(name = "latitudecordinate")
    public String getLatitudecordinate() {
        return latitudecordinate;
    }

    public void setLatitudecordinate(String latitudecordinate) {
        this.latitudecordinate = latitudecordinate;
    }

    @Basic
    @Column(name = "noofslots")
    public int getNoofslots() {
        return noofslots;
    }

    public void setNoofslots(int noofslots) {
        this.noofslots = noofslots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkinglocationEntity that = (ParkinglocationEntity) o;
        return locationId == that.locationId &&
                noofslots == that.noofslots &&
                noofavailableslots == that.noofavailableslots &&
                Objects.equals(name, that.name) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(longitudecordinate, that.longitudecordinate) &&
                Objects.equals(latitudecordinate, that.latitudecordinate) &&
                Objects.equals(rateperhour, that.rateperhour);
    }

    @Override
    public int hashCode() {

        return Objects.hash(locationId, name, companyId, longitudecordinate, latitudecordinate, noofslots, noofavailableslots, rateperhour);
    }
}