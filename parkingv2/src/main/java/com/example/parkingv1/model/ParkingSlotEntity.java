package com.example.parkingv1.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "parkingslot", schema = "mora", catalog = "mora_test")
public class ParkingSlotEntity {
    private int slotId;
    private Integer slotNo;
    private String rateperhour;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Id
    @Column(name = "slotid")
    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    @Basic
    @Column(name = "slotno")
    public Integer getSlotNo() {
        return slotNo;
    }

    public void setSlotNo(Integer slotNo) {
        this.slotNo = slotNo;
    }
    @Basic
    @Column(name = "rateperhour")
    public String getRateperhour() {
        return rateperhour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingSlotEntity that = (ParkingSlotEntity) o;
        return slotId == that.slotId &&
                Objects.equals(slotNo, that.slotNo) &&
                Objects.equals(rateperhour, that.rateperhour);
    }

    @Override
    public int hashCode() {

        return Objects.hash(slotId, slotNo, rateperhour);
    }

    public void setRateperhour(String rateperhour) {
        this.rateperhour = rateperhour;
    }

}
