package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "users", schema = "mora")
public class UsersEntity {
    private String name;
    private String username;
    private String password;
    private String contactno;
    private String nic;
    private String email;
    private Boolean arrivalStatus;
    private Boolean blackListStatus;
    private Integer avoidedCount;
    private int userid;
    private String role;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "contactno")
    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    @Basic
    @Column(name = "nic")
    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Basic
    @Column(name = "arrivalstatus")
    public Boolean getArrivalStatus() {
        return arrivalStatus;
    }

    public void setArrivalStatus(Boolean arrivalStatus) {
        this.arrivalStatus = arrivalStatus;
    }

    @Basic
    @Column(name = "blackliststatus")
    public Boolean getBlackListStatus() {
        return blackListStatus;
    }

    public void setBlackListStatus(Boolean blackListStatus) {
        this.blackListStatus = blackListStatus;
    }
    @Basic
    @Column(name = "avoidedcount")
    public Integer getAvoidedCount() {
        return avoidedCount;
    }

    public void setAvoidedCount(Integer avoidedCount) {
        this.avoidedCount = avoidedCount;
    }
  @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
 @Id
    @Column(name = "userid")
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (userid != that.userid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (contactno != null ? !contactno.equals(that.contactno) : that.contactno != null) return false;
        if (nic != null ? !nic.equals(that.nic) : that.nic != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (arrivalStatus != null ? !arrivalStatus.equals(that.arrivalStatus) : that.arrivalStatus != null)
            return false;
        if (blackListStatus != null ? !blackListStatus.equals(that.blackListStatus) : that.blackListStatus != null)
            return false;
        if (avoidedCount != null ? !avoidedCount.equals(that.avoidedCount) : that.avoidedCount != null) return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (contactno != null ? contactno.hashCode() : 0);
        result = 31 * result + (nic != null ? nic.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (arrivalStatus != null ? arrivalStatus.hashCode() : 0);
        result = 31 * result + (blackListStatus != null ? blackListStatus.hashCode() : 0);
        result = 31 * result + (avoidedCount != null ? avoidedCount.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);

        result = 31 * result + userid;
        return result;
    }
}
