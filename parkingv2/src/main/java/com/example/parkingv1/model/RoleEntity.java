package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "role", schema = "mora", catalog = "mora_test")
public class RoleEntity {
    private int roleid;
    private String userrole;

    @Id
    @Column(name = "roleid")
    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Basic
    @Column(name = "userrole")
    public String getUserrole() {
        return userrole;
    }

    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleEntity that = (RoleEntity) o;

        if (roleid != that.roleid) return false;
        if (userrole != null ? !userrole.equals(that.userrole) : that.userrole != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid;
        result = 31 * result + (userrole != null ? userrole.hashCode() : 0);
        return result;
    }
}
