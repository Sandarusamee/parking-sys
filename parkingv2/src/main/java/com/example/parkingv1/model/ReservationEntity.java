package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "reservation", schema = "mora")
public class ReservationEntity {
    private int reservationId;
    private String startTime;
    private String endTime;
    private String extendtime;
    private String date;
    private int userid;
    private int locationid;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Id
    @Column(name = "reservationid")
    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    @Basic
    @Column(name = "date")
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    @Basic
    @Column(name = "userid")
    public int getUserid() {
        return userid;
    }
    public void setUserid(int userid) {
        this.userid = userid;
    }
    @Basic
    @Column(name = "starttime")
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    @Basic
    @Column(name = "locationid")
    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    @Basic
    @Column(name = "endtime")
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "extendtime")
    public String getExtendtime() {
        return extendtime;
    }

    public void setExtendtime(String extendtime) {
        this.extendtime = extendtime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReservationEntity that = (ReservationEntity) o;

        if (reservationId != that.reservationId) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (extendtime != null ? !extendtime.equals(that.extendtime) : that.extendtime != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reservationId;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (extendtime != null ? extendtime.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);

        return result;
    }
}
