package com.example.parkingv1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "custmrgetsparkinglocation", schema = "mora", catalog = "mora_test")
public class CustmrgetsparkinglocationEntity {
    private int locationId;

    @Id
    @Column(name = "locationId")
    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustmrgetsparkinglocationEntity that = (CustmrgetsparkinglocationEntity) o;

        if (locationId != that.locationId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return locationId;
    }
}
