package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "customerratespklocation", schema = "mora", catalog = "mora_test")
public class CustomerratespklocationEntity {
    private Double rate;
    private int rateid;

    @Basic
    @Column(name = "rate")
    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Id
    @Column(name = "rateid")
    public int getRateid() {
        return rateid;
    }

    public void setRateid(int rateid) {
        this.rateid = rateid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerratespklocationEntity that = (CustomerratespklocationEntity) o;

        if (rateid != that.rateid) return false;
        if (rate != null ? !rate.equals(that.rate) : that.rate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rate != null ? rate.hashCode() : 0;
        result = 31 * result + rateid;
        return result;
    }
}
