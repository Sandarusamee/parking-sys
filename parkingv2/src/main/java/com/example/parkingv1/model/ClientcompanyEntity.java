package com.example.parkingv1.model;

import javax.persistence.*;

@Entity
@Table(name = "clientcompany", schema = "mora", catalog = "mora_test")
public class ClientcompanyEntity {
    private Integer registrationNo;
    private String name;
    private String contactNo;
    private String userName;
    private String password;
    private String email;
    private Boolean blacklistDetails;
    private Integer noOfSlots;
    private int companyid;
    private String role;
    @Basic
    @Column(name = "registrationno")
    public Integer getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(Integer registrationNo) {
        this.registrationNo = registrationNo;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.name = name;
    }
    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "contactno")
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Basic
    @Column(name = "username")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "blacklistdetails")
    public Boolean getBlacklistDetails() {
        return blacklistDetails;
    }

    public void setBlacklistDetails(Boolean blacklistDetails) {
        this.blacklistDetails = blacklistDetails;
    }

    @Basic
    @Column(name = "noofslots")
    public Integer getNoOfSlots() {
        return noOfSlots;
    }

    public void setNoOfSlots(Integer noOfSlots) {
        this.noOfSlots = noOfSlots;
    }
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Id
    @Column(name = "companyid")
    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientcompanyEntity that = (ClientcompanyEntity) o;

        if (companyid != that.companyid) return false;
        if (registrationNo != null ? !registrationNo.equals(that.registrationNo) : that.registrationNo != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (contactNo != null ? !contactNo.equals(that.contactNo) : that.contactNo != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (blacklistDetails != null ? !blacklistDetails.equals(that.blacklistDetails) : that.blacklistDetails != null)
            return false;
        if (noOfSlots != null ? !noOfSlots.equals(that.noOfSlots) : that.noOfSlots != null) return false;
        return role != null ? role.equals(that.role) : that.role == null;
    }

    @Override
    public int hashCode() {
        int result = registrationNo != null ? registrationNo.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (contactNo != null ? contactNo.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (blacklistDetails != null ? blacklistDetails.hashCode() : 0);
        result = 31 * result + (noOfSlots != null ? noOfSlots.hashCode() : 0);
        result = 31 * result + companyid;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
