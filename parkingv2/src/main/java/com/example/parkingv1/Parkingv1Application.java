package com.example.parkingv1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Parkingv1Application {

	public static void main(String[] args) {
		SpringApplication.run(Parkingv1Application.class, args);
	}
}
