package com.example.parkingv1.controller;

import com.example.parkingv1.dto.UserDTO;
import com.example.parkingv1.model.ClientcompanyEntity;

import com.example.parkingv1.model.ParkinglocationEntity;
import com.example.parkingv1.model.ReservationEntity;
import com.example.parkingv1.model.UsersEntity;
import com.example.parkingv1.repository.*;
import com.example.parkingv1.service.CalculateBill;
import com.example.parkingv1.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class webController {
    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public String reg(Model model) {
        model.addAttribute("users", new UsersEntity());
        return "register";
    }

    @Autowired
    UserRepo usersRepo;
UsersEntity usersEntity;
    @RequestMapping(value = "/regform", method = RequestMethod.POST)
    public String regform(@ModelAttribute UsersEntity users) {
        if (usersRepo.findUserByUsername(users.getUsername()) == null) {
           users.setRole("ROLE_USER");
            // UsersEntity usrbuff = usersRepo.save(users);
            //users.setUserid(usrbuff.getUserid());
            //usersRepo.save(users);
            usersRepo.save(users);
            return "searchpark1";
        } else {
            return "register";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        // model.addAttribute("users", new UsersEntity());
        return "Login";
    }

    @RequestMapping(value = "/map", method = RequestMethod.GET)
    public String map(Model model) {
        // model.addAttribute("users", new UsersEntity());
        return "map";
    }


    /*  @RequestMapping(value="/loged", method= RequestMethod.POST)
      public String userLogedin(@ModelAttribute UsersEntity users) {
          if (users.getPassword()==users.getUsername())
              return "loged";
          else
              return "loginfail";
      }*/
   /* @RequestMapping(value="/loged", method = RequestMethod.POST)
    public String loged(@ModelAttribute UsersEntity users){

        UsersEntity a = new UsersEntity();
        a.getPassword();
        a.getUsername();


        boolean isValidUser = UserDetailService.validateUser(users.getUsername(), users.getPassword());

        if (!isValidUser) {
        //  model.put("errorMessage", "Invalid Credentials");
            return "Login";
        }

       // model.put("name", name);
      //  model.put("password", password);
        return "loged";
    }*/

ReservationEntity reservationEntity;
    @RequestMapping(value = "/booking", method = RequestMethod.GET)
    public String booking( Model model) {
        model.addAttribute("reservation", new ReservationEntity());

        return "booking";
    }

    @Autowired
    ReservationRepo reservationRepo;

    @RequestMapping(value = "/booked", method = RequestMethod.POST)
    public String booking(@ModelAttribute ReservationEntity reservation) {
        //if (reservationRepo.findReservationEntitiesByStartTime(reservation.getStartTime()) == null) {
            // UsersEntity usrbuff = usersRepo.save(users);
            //users.setUserid(usrbuff.getUserid());
            //usersRepo.save(users);


            reservationRepo.save(reservation);
            return "booksuccess";
     //   } else {
        //
        //    return "bookingFail";
        }

    @RequestMapping(value = "/bookedlist", method = RequestMethod.GET)
    public List<ReservationEntity> bookinglist(@ModelAttribute ReservationEntity reservation) {
        //  reservationRepo.findAllByDate(reservation.getDate());
        List<ReservationEntity> a = new ArrayList();
        reservationRepo.findAllByDate(reservation.getDate());


        return a;
    }

    @RequestMapping(value = "/userslist", method = RequestMethod.GET)
    public String OngoingVehiclesGarage(@RequestParam("users") String user, Model model) {

        //UsersEntity userL = usersRepo.findUserByUsername(user);
        //Garage ga = garageRepository.findOne(userE.getId());
        List<UsersEntity> userList = (List<UsersEntity>) usersRepo.findAll();
        model.addAttribute("uList", userList);

        return "userList";
    }

    @RequestMapping(value = "/parking", method = RequestMethod.GET)
    public String HomePage() {

        return "indexx";
    }

    @Autowired
    CompanyRepo companyRepo;

    @RequestMapping(value = "/company", method = RequestMethod.GET)
    public String comreg(Model model) {
        model.addAttribute("clientcompany", new ClientcompanyEntity());
        return "companyreg";
    }

    @RequestMapping(value = "/comreged", method = RequestMethod.POST)
    public String comreged(@ModelAttribute ClientcompanyEntity clientcompany, UsersEntity users) {
        if (companyRepo.findByName(clientcompany.getName()) == null) {
            users.setRole("ROLE_COMPANY");
            users.setUsername(clientcompany.getName());
            usersRepo.save(users);
            companyRepo.save(clientcompany);
            return "bookingSuccess";
        } else {
            return "bookingFail";
        }
    }

    @Autowired
    ParkingLocationRepo parkingLocationRepo;

    @RequestMapping(value = "/parkadd", method = RequestMethod.GET)
    public String prkadd(Model model) {
        model.addAttribute("parkinglocation", new ParkinglocationEntity());
        return "mapg";
    }

    @RequestMapping(value = "/prkadded", method = RequestMethod.POST)
    public String prkadded(@ModelAttribute ParkinglocationEntity parkinglocation) {

        parkingLocationRepo.save(parkinglocation);
        return "xmldash";


    }

  /*  @RequestMapping(path = "/new_topic", method = RequestMethod.POST)
    public ResponseEntity new_topic(@ModelAttribute ParkinglocationEntity topic) throws Exception {


        parkingLocationRepo.save(topic);
        HttpHeaders headers = new HttpHeaders();
        headers.add("success", "topic added");
        return new ResponseEntity(headers, HttpStatus.OK);
    }*/


    @RequestMapping(value = "/seeusers", method = RequestMethod.GET)
    public String seeUsers(Model model) {

        UsersEntity users = usersRepo.findOne(15);
        model.addAttribute("users", users);

        //model.addAttribute("users",usersRepo.findAll());
        return "userinfo";
    }

    @RequestMapping(value = "/userlist", method = RequestMethod.GET)
    public String seeAllUsers(Model model) {


        model.addAttribute("users", usersRepo.findAll());

        //model.addAttribute("users",usersRepo.findAll());
        return "userList";
    }
/*
    @RequestMapping(value = "/test2", method = RequestMethod.GET)
    public String showPrks(Model model2) {


        model2.addAttribute("parking", parkingLocationRepo.findAll());


        return "maptest1";
    }*/
@Autowired
CalculateBill calculateBill;
 @Autowired
    CurrentUserService currentUserService;
    @Autowired
    ParkingSlotRepo parkingSlotRepo;
    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    public String bill(Model model) {
        try {
            model.addAttribute("bill", calculateBill.calculation(1,1));
            model.addAttribute("users",usersRepo.findOne(calculateBill.getUserid(5)));
            model.addAttribute("slot",parkingSlotRepo.findOne(1));
            model.addAttribute("reservation",calculateBill.reservedTime(1));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "bill";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String getlocation(Model model) {


      //  model.addAttribute("parking", parkingLocationRepo.findAll());


        return "test22";
    }

    UserDTO userDTO;
    @RequestMapping(value = "/test4", method = RequestMethod.GET)
    public String searchusers (){


        return "searchparking";
    }
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchusersfinal(@RequestParam("input")String input ,Model model) {
       UsersEntity buff= usersRepo.findUserByUsernamee(input);
        model.addAttribute("input", buff);
        return "searchedusers";
    }



    @RequestMapping(value = "/searchcompany", method = RequestMethod.GET)
    public String searchCompany() {


        return "xmldash";
    }
    @RequestMapping(value = "/searchxcompany", method = RequestMethod.POST)
    public String searchCompany( @RequestParam("namee") String name, Model model) throws Exception {
        try {


            ClientcompanyEntity clientcompany = companyRepo.findByName(name);
            model.addAttribute("namee", clientcompany);

            //model.addAttribute("users",usersRepo.findAll());
            return "xxmldash";
        } catch (NullPointerException a){
            return "xmldash";
        }


    }

    @RequestMapping(value = "/editcompany", method = RequestMethod.GET)
    public String editCompany() {


        return "xxxmldash";
    }

    @RequestMapping(value = "/searchslots", method = RequestMethod.GET)
    public String searchSlots() {


        return "xmldash2";
    }

    @RequestMapping(value = "/searchxslot", method = RequestMethod.POST)
    public String searchSlots( @RequestParam("namee") String name, Model model) {


        ClientcompanyEntity clientcompany = companyRepo.findByName(name);
        model.addAttribute("namee", clientcompany);

        //model.addAttribute("users",usersRepo.findAll());
        return "xxmldash2";

    }


    @RequestMapping(value = "/searchcharges", method = RequestMethod.GET)
    public String searchCharges() {


        return "xmldash3";
    }

}