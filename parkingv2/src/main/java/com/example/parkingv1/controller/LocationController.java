package com.example.parkingv1.controller;

import com.example.parkingv1.dto.LocationDTO;
import com.example.parkingv1.model.ParkinglocationEntity;
import com.example.parkingv1.model.UsersEntity;
import com.example.parkingv1.repository.ParkingLocationRepo;
import com.example.parkingv1.repository.ParkingSlotRepo;
import com.example.parkingv1.service.GetLonLat;
import com.example.parkingv1.service.NearestParking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class LocationController {
    @Autowired
    LocationDTO locationDTO;

    @RequestMapping(value = "/getlocation",method = RequestMethod.GET)
        public String getlocation(Model model){


        return "map";



        }

/*
    @RequestMapping(value = "/controll", method = RequestMethod.POST)
    public String locationadd(@RequestParam double longitude, @RequestParam double latitude,Model model )
{    int[] id=nearestParking.getloids();
    double finallats[]=new double[id.length];
    double finallons[]=new double[id.length];
    locationDTO.setCurrentlat(latitude);
    locationDTO.setCurrentlon(longitude);

    finallats = getLonLat.matchedlat();
    finallons = getLonLat.matchedlot();

    model.addAttribute("parking",finallats[1]);
   // model.addAttribute("parkingg",finallons[1]);

    return "maptest11";



}*/

    @Autowired
    NearestParking nearestParking;
    @Autowired
    GetLonLat getLonLat;
    @Autowired
    ParkingLocationRepo parkingLocationRepo;

/*
    @RequestMapping(value = "/test3", method = RequestMethod.GET)
    public String Distance(Model model) {
        int i;
        int[] id={0,1,2,3};
        double [] distance= new double[4];
        for(i=0;i<4;i++) {

              distance[i]=  nearestParking.nearestParking(id[i],30, 80);


        }


        model.addAttribute("distance",distance [0]);
         model.addAttribute("diss", distance[1]);

        return "distancedata";
    }*/
    @RequestMapping(value = "/searchpark", method = RequestMethod.GET)
    public String searchParks () throws Exception{


        return "searchpark1";
    }

    @RequestMapping(value = "/searchedparks", method = RequestMethod.POST)
    public String showParkings(@RequestParam ("latitude")String latidude ,@RequestParam("longitude")String longitude ,Model model) {

        int[] id=nearestParking.getloids(Double.parseDouble(latidude),Double.parseDouble(longitude));


        ParkinglocationEntity[] parks=new ParkinglocationEntity[id.length];
        double [] distances = new double[id.length];

       String [] rateperhour = new String[id.length];
       int i;
       int j=0;
        for(i=0;i<id.length;i++) {
        // if(id[i]!=0) {
               parks[i] = parkingLocationRepo.findOne(id[i]);
               distances[i] = nearestParking.nearestParking(id[i], Double.parseDouble(latidude), Double.parseDouble(longitude));
         //  }
        }

            model.addAttribute("parking",parks);
            model.addAttribute("distance",distances);
            model.addAttribute("id",id);

        return "customerViewNearestParkings";
    }





}

