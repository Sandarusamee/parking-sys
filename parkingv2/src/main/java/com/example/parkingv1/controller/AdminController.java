package com.example.parkingv1.controller;

import com.example.parkingv1.dto.UserDTO;
import com.example.parkingv1.model.ClientcompanyEntityApproved;
import com.example.parkingv1.model.UsersEntity;
import com.example.parkingv1.repository.CompanyApprovedRepository;
import com.example.parkingv1.repository.CompanyRepo;
import com.example.parkingv1.repository.UserRepo;
import com.example.parkingv1.service.AdminServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    @Autowired
    UserRepo usersRepo;

    @Autowired
    CompanyRepo companyRepo;

    @Autowired
    CompanyApprovedRepository companyApprovedRepository;

    UserDTO userDTO;


    @RequestMapping("/index")
    public String index(){
        return "adminindex";
    }


    /*@RequestMapping("/viewusersss")
    public String viewusers(){
        return "adminviewusers";
    } */

    @RequestMapping("/viewusers")
    public String viewusers2(){
        return "adminviewusers2";
    }


    @RequestMapping("/adminviewusers_viewallusers")
    public String viewallusers(Model model)

    {
        try {
            model.addAttribute("users", usersRepo.findAll());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "adminviewusers_viewallusers";
    }



    @RequestMapping(value = "/deletecustomerbyid", method = RequestMethod.POST)
    public String deleteuserbyid( @RequestParam("userid") int userid, Model model  )
    {
        UsersEntity userbuffer3= usersRepo.findUserByUserid(userid);
        model.addAttribute("users", userbuffer3);

        usersRepo.delete(userid); //working
        return "adminviewusers_deletecustomerbyid";
    }





    @RequestMapping(value = "/updatecustomerbyid_submitform", method = RequestMethod.POST)
    public String  updateuserbyidform (@RequestParam("userid") int userid, Model model)
    {
        UsersEntity userbuffer5= usersRepo.findUserByUserid(userid); //take from db and assign to buffer
        model.addAttribute("users", userbuffer5);
        return "adminviewusers_updatecustomerbyid_submitform";
    }






    @RequestMapping(value = "/updatecustomerbyid_newtable", method = RequestMethod.POST)
    public String updateuserbyid(@RequestParam("userid") int userid, @ModelAttribute UsersEntity usersEntity, Model model )
    {
        usersRepo.save(usersEntity);

        UsersEntity userbuffer6= usersRepo.findUserByUserid(userid);
        model.addAttribute("users", userbuffer6);

        return "adminviewusers_updatecustomerbyid_newtable";
    }













    @RequestMapping(value="/adminviewusers_viewusersbyid", method = RequestMethod.POST)
    public String viewuserbyid(@RequestParam("userid") int userid, Model model ) //take search input
    {
        UsersEntity userbuffer1= usersRepo.findUserByUserid(userid); //take from db and assign to buffer
        model.addAttribute("users", userbuffer1);
        return "adminviewusers_viewuserbyid";
    }

    @RequestMapping(value="/adminviewusers_viewusersbyname", method = RequestMethod.POST)

    public String viewuserbyname(@RequestParam("name") String name, Model model ) //serach input
    {

        UsersEntity userbuffer2= usersRepo.findUserByName(name); //take from db
        model.addAttribute("users2", userbuffer2);

        return "adminviewusers_viewuserbyname";
    }





    @RequestMapping("/adminviewusers_viewallcompanies")
    public String viewallcomapanies(Model model)

    {
        try {
            model.addAttribute("companies", companyRepo.findAll());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "adminviewusers_viewallcompanies";
    }





    @RequestMapping("/viewres")
    public String viewres(){
        return "adminviewres";
    }


    @RequestMapping("/comapproval")
    public String comapproval(){
        return "admincomapproval";
    }

    @RequestMapping("/viewregisteredcompanies")
    public String viewregisteredcompanies(Model model)
    {
        try {
            model.addAttribute("companies", companyRepo.findAll());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "admincomapproval_viewregisteredcompanies";
    }





    @Autowired
    AdminServices adminServices;


    @RequestMapping(value = "/acceptregisteredcompanies", method = RequestMethod.POST)
    public String acceptregisteredcompanies
            ( @RequestParam("registrationNo") int registrationNo,
              @ModelAttribute ClientcompanyEntityApproved clientcompanyEntityApproved)
    {

     //   companyApprovedRepository.save(clientcompanyEntityApproved);
     //   adminServices.addToPermenantTable(registrationNo);

        return "actionsucces";
    }




    @RequestMapping("/slotupdates")
    public String slotupdates() {
        return "adminslotupdates";
    }


    @RequestMapping("/viewadminindex")
    public String adminlogin() {
        return "adminindex";
    }




}
